A simple rust + gtk demo using gtk-rs. See https://github.com/gtk-rs/examples 
for more examples from the developers. This program also used glade for the 
GUI layout/design.

The program is just a simple hello world program. It is composed of two buttons
that reset and display a message in a gtk label widget. 

**To run on Linux:**

`git clone https://gitlab.com/apattyn/gtkrs_hello_world.git`

`cd gtkrs_hello_world`

`cargo run`

**To do:**

Package it as a flatpak.