use gio::prelude::*;
use gtk::prelude::*;

use gtk::{Window, Builder, Button, Label};

use std::env::args;

fn main() {

    let app_id = "com.gitlab.apattyn.gtkdemo";
    let app = gtk::Application::new(
                Some(&app_id),
                Default::default(),)
                .expect("Initialization of GTK app failed!");

    app.connect_activate(|myapp| {
        glade_ui(&myapp);
    });

    app.run(&args().collect::<Vec<_>>());

    println!("Exited sucessfully!")
}

fn glade_ui(app: &gtk::Application){
    // Read in the glade source file.
    let glade_src = include_str!("gui/example.glade");
    let builder = Builder::new_from_string(glade_src);

    // Get objects from glade file.
    let window:Window = builder.get_object("window")
                                    .expect("Couldn't get 'winow' from glade src file.");
    window.set_application(Some(app));
    let display_button:Button = builder.get_object("display_button")
                                .expect("Couldn't get the display button from the glade src file.");
    let reset_button:Button = builder.get_object("reset_button")
                                .expect("Couldn't get the reset button from the glade src file.");
    let label1:Label = builder.get_object("label1")
                        .expect("Couldn't get 'label1' from the glade src file.");

    // Set-up for the display button.
    let label1_display = label1.clone();
    display_button.connect_clicked(move |_| {
        label1_display.set_text("Hello World!");
    });

    // Set-up for the reset button.
    let label1_reset = label1.clone();
    reset_button.connect_clicked(move |_| {
        label1_reset.set_text("Press the diplay button!");
    });

    window.show_all()
}
